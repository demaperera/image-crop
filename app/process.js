function crop(url, filename, type) {
  input(url, filename, type, function(result){});
}

function input(url, filename, type, callback) {

    var fs = require('fs'),
        request = require('request');

    var download = function(uri, filename, callback){
        request.head(uri, function(err, res, body){
            console.log('[ok] content-type:', res.headers['content-type']);
            console.log('[ok] content-length:', res.headers['content-length']);
            request(uri).pipe(fs.createWriteStream("source/" + filename + ".jpeg")).on('close', callback);
        });
    };

    download(url, filename, function(){
        var Canvas = require('canvas');
        var image = new Canvas.Image();
        image.src = "source/" + filename + ".jpeg";
        removeWhitespace(image, type, filename);
    });

}

function removeWhitespace(image, type, filename){

    var Canvas = require('canvas'),
        canvas = new Canvas(image.width, image.height),
        context = canvas.getContext('2d'),
        fs = require('fs');

    const width = image.width, height = image.height;

    canvas.width = width;
    canvas.height = height;

    context.width = width;
    context.height = height;
    context.drawImage(image, 0, 0, width, height);

    const imageData = context.getImageData(0, 0, canvas.width, canvas.height);

    for (x = 0; x < imageData.width * imageData.height * 4; x += 4) {
        var red = imageData.data[x + 0],
            green = imageData.data[x + 1],
            blue = imageData.data[x + 2],
            alpha = imageData.data[x + 3];
        selectedRed = 240;
        selectedGreen = 240;
        selectedBlue = 240;
        if (red >= selectedRed && green >= selectedGreen && blue >= selectedBlue) {
            imageData.data[x + 3] = 0;
        }
    }

    context.putImageData(imageData, 0, 0);
    var croppedImage = cropImage(context,canvas, type);
    var out = fs.createWriteStream(__dirname + '/cropped/' + filename + '.png')
      , stream = canvas.createPNGStream();

    stream.on('data', function(chunk){
      out.write(chunk);
    });
    return croppedImage;

}

function cropImage(context, canvas, type) {

    var w = canvas.width,
        h = canvas.height,
    pix = {x:[], y:[]},
    imageData = context.getImageData(0,0,canvas.width,canvas.height),
    x, y, index;

    for (y = 0; y < h; y++) {
        for (x = 0; x < w; x++) {
            index = (y * w + x) * 4;
            if (imageData.data[index + 3] > 0) {
                pix.x.push(x);
                pix.y.push(y);
            }
        }
    }


    pix.x.sort(function(a,b){return a-b});
    pix.y.sort(function(a,b){return a-b});
    var n = pix.x.length-1;

    w = pix.x[n] - pix.x[0];
    h = pix.y[n] - pix.y[0];

    var cut = context.getImageData(pix.x[0], pix.y[0], w, h);

    canvas.width = w;
    canvas.height = h;

    if(type=="solid") {
      for(var i=0; i < cut.data.length; i += 4){
          if(cut.data[i+3]<255){
              cut.data[i] = 255;
              cut.data[i+1] = 255;
              cut.data[i+2] = 255;
              cut.data[i+3] = 255;
          }
      }
    }

    context.putImageData(cut, 0, 0);
    return canvas.toDataURL('image/png');

}

module.exports.crop = crop
